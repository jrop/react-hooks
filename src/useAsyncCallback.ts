import { useRef } from "react";

import useWrappedCallback from "./useWrappedCallback";
import useMounted from "./useMounted";

type ExtractProimse<T> = T extends Promise<infer U> ? U : T;

export const useAsyncCallback = <
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  F extends (...args: any[]) => Promise<any>,
>(
  f: F,
  opts?: {
    onError?: (e: unknown) => void;
    onOk?: (value: ExtractProimse<ReturnType<F>>) => void;
  },
) => {
  const fnWrapped = useWrappedCallback(f);
  const mounted = useMounted();
  const optsRef = useRef(opts);
  optsRef.current = opts;

  const callback = useWrappedCallback(async (...args: Parameters<F>) => {
    try {
      const result = await fnWrapped(...args);
      mounted.current &&
        optsRef.current?.onOk?.(result as ExtractProimse<ReturnType<F>>);
      return result;
    } catch (e) {
      mounted.current && optsRef.current?.onError?.(e);
    }
  });

  return callback as F;
};
