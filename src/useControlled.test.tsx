/* eslint-disable @typescript-eslint/ban-types */
/// <reference lib="dom" />
import { it, expect, beforeEach, afterEach } from "bun:test";

import React from "react";
import { createRoot, Root } from "react-dom/client";
import { act } from "@testing-library/react";
import useControlled from "./useControlled";

let container: HTMLElement | undefined;
let root: Root | undefined;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
  root = createRoot(container);
});
afterEach(() => {
  container!.remove();
  container = undefined;
});

const MyComponent = (props: {
  num?: number;
  onNumChange?: (n: number) => unknown;
  setNumRef?: (setNum: React.Dispatch<React.SetStateAction<number>>) => unknown;
}) => {
  const [num, setNum] = useControlled(props.num, 0, props.onNumChange);
  props.setNumRef?.(setNum);
  return <React.Fragment>{num}</React.Fragment>;
};

it("should render", () => {
  act(() => {
    root!.render(<MyComponent />);
  });
});

it("should setState", () => {
  let setNum: React.Dispatch<React.SetStateAction<number>>;
  act(() => {
    root!.render(<MyComponent setNumRef={(s) => (setNum = s)} />);
  });
  expect(container?.textContent).toBe("0");

  act(() => {
    setNum(2);
  });
  expect(container?.textContent).toBe("2");
});

it("should derive from prop", () => {
  act(() => {
    root!.render(<MyComponent num={3} />);
  });
  expect(container?.textContent).toBe("3");
});

it("should follow prop & setNumRef should always be the same function", () => {
  let setNumRef1: Function;
  act(() => {
    root!.render(<MyComponent num={3} setNumRef={(f) => (setNumRef1 = f)} />);
  });
  expect(container?.textContent).toBe("3");

  let setNumRef2: Function;
  act(() => {
    root!.render(<MyComponent num={4} setNumRef={(f) => (setNumRef2 = f)} />);
  });
  expect(setNumRef1!).toBe(setNumRef2!);
  expect(container?.textContent).toBe("4");
});

it("should call onChange (uncontrolled)", () => {
  let setState: React.Dispatch<React.SetStateAction<number>>;
  const calls = [] as number[];
  const onNumChange = (n: number) => calls.push(n);
  act(() => {
    root!.render(
      <MyComponent
        setNumRef={(s) => (setState = s)}
        onNumChange={onNumChange}
      />,
    );
  });
  expect(container?.textContent).toBe("0");
  expect(calls).toStrictEqual([]);

  act(() => {
    setState(1);
  });
  expect(container?.textContent).toBe("1");
  expect(calls).toStrictEqual([1]);

  act(() => {
    setState(2);
  });
  expect(container?.textContent).toBe("2");
  expect(calls).toStrictEqual([1, 2]);
});

it("should call onChange (controlled)", () => {
  let setState: React.Dispatch<React.SetStateAction<number>>;
  const calls = [] as number[];
  const onNumChange = (n: number) => calls.push(n);
  act(() => {
    root!.render(
      <MyComponent
        setNumRef={(s) => (setState = s)}
        num={1}
        onNumChange={onNumChange}
      />,
    );
  });
  expect(container?.textContent).toBe("1");
  expect(calls).toStrictEqual([]);

  act(() => {
    setState(2);
  });
  // since we are in controlled mode, we expect the
  // HTML to _stay_ at "1", as the prop did not change.
  expect(container?.textContent).toBe("1");
  expect(calls).toStrictEqual([2]);
});

it("should throw when changing from uncontrolled => controlled", () => {
  act(() => {
    root!.render(<MyComponent />);
  });
  expect(container?.textContent).toBe("0");

  expect(() => {
    act(() => {
      root!.render(<MyComponent num={1} />);
    });
  }).toThrow(/Cannot switch from uncontrolled => controlled/);
});

it("should throw when changing from controlled => uncontrolled", () => {
  act(() => {
    root!.render(<MyComponent num={1} />);
  });
  expect(container?.textContent).toBe("1");

  expect(() => {
    act(() => {
      root!.render(<MyComponent />);
    });
  }).toThrow(/Cannot switch from controlled => uncontrolled/);
});
