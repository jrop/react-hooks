import React from "react";
import useWrappedCallback from "./useWrappedCallback";
import { usePropHistory } from "./usePropHistory";
import { useIsFirstRender } from "./useIsFirstRender";

export const useControlledReducer = <A, T>(
  prop: T | undefined,
  initialValue: T | (() => T),
  reducer: (prevState: T, action: A) => T,
  onChange?: (x: T) => unknown,
) => {
  const [isControlled, wasControlled] = usePropHistory(
    typeof prop !== "undefined",
  );
  if (typeof wasControlled !== "undefined" && wasControlled !== isControlled) {
    const str = (isControlled: boolean) =>
      isControlled ? "controlled" : "uncontrolled";
    throw new Error(
      `Cannot switch from ${str(wasControlled)} => ${str(isControlled)}`,
    );
  }

  const isFirstRender = useIsFirstRender();
  const [state, dispatch] = React.useReducer(reducer, undefined, () =>
    typeof initialValue === "function"
      ? // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (initialValue as () => any)()
      : initialValue,
  );
  React.useEffect(() => {
    if (isControlled || isFirstRender) return;
    onChange?.(state);
  }, [isControlled, isFirstRender, state]);

  const dispatchControlled: React.Dispatch<A> = useWrappedCallback(
    React.useCallback(
      (action: A) => {
        const nextState = reducer(state, action);
        onChange?.(nextState);
      },
      [state],
    ),
  );

  return isControlled
    ? ([prop, dispatchControlled] as const)
    : ([state, dispatch] as const);
};
export default useControlledReducer;
