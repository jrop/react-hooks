/* eslint-disable @typescript-eslint/ban-types */
/// <reference lib="dom" />
import { it, expect, beforeEach, afterEach } from "bun:test";

import React from "react";
import { createRoot, Root } from "react-dom/client";
import { act } from "@testing-library/react";
import useWrappedCallback from "./useWrappedCallback";

let container: HTMLElement | undefined;
let root: Root | undefined;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
  root = createRoot(container);
});
afterEach(() => {
  container!.remove();
  container = undefined;
});

const MyComponent = (props: {
  fnRef?: (fn: Function) => unknown;
  innerFnRef?: (fn: Function) => unknown;
}) => {
  const innerFn = () => {};
  const fn = useWrappedCallback(innerFn);
  props.fnRef?.(fn);
  props.innerFnRef?.(innerFn);
  return <React.Fragment>no content</React.Fragment>;
};

it("should render", () => {
  act(() => {
    root!.render(<MyComponent />);
  });
});

it("should always return the same function, even when the inner function changes", () => {
  let fn1: Function;
  let innerFn1: Function;
  act(() => {
    root!.render(
      <MyComponent
        fnRef={(f) => (fn1 = f)}
        innerFnRef={(f) => (innerFn1 = f)}
      />,
    );
  });
  expect(fn1!).toBeInstanceOf(Function);
  expect(innerFn1!).toBeInstanceOf(Function);

  let fn2: Function;
  let innerFn2: Function;
  act(() => {
    root!.render(
      <MyComponent
        fnRef={(f) => (fn2 = f)}
        innerFnRef={(f) => (innerFn2 = f)}
      />,
    );
  });
  expect(fn2!).toBe(fn1!);
  expect(innerFn2!).not.toBe(innerFn1!);
});
