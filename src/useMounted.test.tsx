/// <reference lib="dom" />
import { it, expect, beforeEach, afterEach } from "bun:test";

import React from "react";
import { createRoot } from "react-dom/client";
import { act } from "@testing-library/react";
import useMounted from "./useMounted";

let container: HTMLElement | undefined;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  container!.remove();
  container = undefined;
});

const MyComponent = (props: { mountedRef?: (mounted: boolean) => unknown }) => {
  const mounted = useMounted();
  props.mountedRef?.(mounted.current);
  return <React.Fragment>no content</React.Fragment>;
};

it("should render", () => {
  let mounted = false;
  const captureMounted = (m: boolean) => {
    mounted = m;
  };
  act(() => {
    createRoot(container!).render(<MyComponent mountedRef={captureMounted} />);
  });
  expect(mounted).toBe(true);
});
