import React from "react";
import useMounted from "./useMounted";

export const useAsync = <T>(
  fn: () => Promise<T>,
): {
  data: T | undefined;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  error: any;
  loading: boolean;
  refresh: () => void;
} => {
  const mounted = useMounted();

  const [data, setData] = React.useState<T | undefined>(undefined);
  const [loading, setLoading] = React.useState(true);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [error, setError] = React.useState<any>(undefined);

  const [refreshIdx, setRefetchIdx] = React.useState(0);
  const latestRefreshIdx = React.useRef(refreshIdx);
  latestRefreshIdx.current = refreshIdx;

  React.useEffect(
    () => {
      const myRefreshIdx = refreshIdx;
      setLoading(true);
      fn().then(
        (r) => {
          if (!mounted.current) return;
          if (myRefreshIdx !== latestRefreshIdx.current) return;
          setError(undefined);
          setData(r);
          setLoading(false);
        },
        (e) => {
          setError(e);
          setLoading(false);
        },
      );
    },
    // this hook depends on this array of "things", and whenever one or more of
    // the things in this dependency array changes, the effect reruns. We use
    // this trick to implement "refreshing"
    [mounted, refreshIdx],
  );

  const refresh = React.useCallback(() => {
    // cause the effect to run again by changing something it depends on:
    setRefetchIdx(refreshIdx + 1);
  }, [refreshIdx]);

  return {
    data,
    loading,
    error,
    refresh,
  };
};
export default useAsync;
