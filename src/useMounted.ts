import React, { useRef } from "react";

export const useMounted = () => {
  const mounted = useRef(true);
  React.useEffect(() => () => void (mounted.current = false), []);
  return mounted;
};
export default useMounted;
