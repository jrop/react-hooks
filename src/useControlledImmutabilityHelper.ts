import update, { Spec } from "immutability-helper";
import useControlledReducer from "./useControlledReducer";

export const useControlledImmutabilityHelper = <T>(
  prop: T | undefined,
  initialValue: T | (() => T),
  onChange?: (x: T) => unknown,
) =>
  useControlledReducer(
    prop,
    initialValue,
    (prev, action: Spec<T>) => update(prev, action),
    onChange,
  );
export default useControlledImmutabilityHelper;
