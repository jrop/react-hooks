import { useRef } from "react";

export const usePropHistory = <T>(prop: T, opts?: { size?: number }) => {
  const size = opts?.size ?? 1;

  const history = useRef<T[]>([]);

  const last = history.current[0];
  if (last !== prop) {
    history.current.unshift(prop);
    if (history.current.length > size + 1) {
      history.current.pop();
    }
  }

  return [prop, ...history.current.slice(1)] as const;
};
