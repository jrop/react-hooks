import React from "react";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Func = (...args: any[]) => any;

export const useWrappedCallback = <F extends Func>(f: F) => {
  const fn = React.useRef(f);
  fn.current = f;
  return React.useCallback(
    (...args: Parameters<F>) => fn.current(...args),
    [],
  ) as F;
};
export default useWrappedCallback;
