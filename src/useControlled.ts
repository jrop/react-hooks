import React from "react";
import useWrappedCallback from "./useWrappedCallback";
import { usePropHistory } from "./usePropHistory";
import { useIsFirstRender } from "./useIsFirstRender";

export const useControlled = <T>(
  prop: T | undefined,
  initialValue: T | (() => T),
  onChange?: (x: T) => unknown,
) => {
  const [isControlled, wasControlled] = usePropHistory(
    typeof prop !== "undefined",
  );
  if (typeof wasControlled !== "undefined" && wasControlled !== isControlled) {
    const str = (isControlled: boolean) =>
      isControlled ? "controlled" : "uncontrolled";
    throw new Error(
      `Cannot switch from ${str(wasControlled)} => ${str(isControlled)}`,
    );
  }

  const isFirstRender = useIsFirstRender();
  const [state, setState] = React.useState(initialValue);
  React.useEffect(() => {
    if (isControlled || isFirstRender) return;
    onChange?.(state);
  }, [isControlled, isFirstRender, state]);

  const setStateControlled: React.Dispatch<React.SetStateAction<T>> =
    useWrappedCallback((action: React.SetStateAction<T>) => {
      const nextState =
        typeof action === "function"
          ? (action as (prev: T) => T)(state)
          : action;
      onChange?.(nextState);
    });

  return isControlled
    ? ([prop, setStateControlled] as const)
    : ([state, setState] as const);
};
export default useControlled;
