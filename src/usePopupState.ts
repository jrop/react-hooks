import React from "react";

type FResolve<T> = (result: T) => void;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type FReject = (reason: any) => void;

function defer<T>() {
  let resolve: FResolve<T>;
  let reject: FReject;
  const defer: Promise<T> = new Promise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });
  return Object.assign(defer, { resolve: resolve!, reject: reject! });
}

export const usePopupState = <T, R>(initialState?: T) => {
  const deferred = React.useRef(defer<R>());
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState(initialState);

  // The following functions do not need to stay
  // "fresh" because the only vars they close over
  // do not go stale:
  const memoizedFunctions = React.useMemo(() => {
    const showForResult = (initialState?: T) => {
      if (typeof initialState !== "undefined") setState(initialState);

      setOpen(true);
      deferred.current = defer<R>();
      return deferred.current as Promise<R>;
    };
    const resolve: FResolve<R> = (result) => {
      deferred.current?.resolve(result);
      setOpen(false);
    };
    const reject: FReject = (reason) => {
      deferred.current?.reject(reason);
      setOpen(false);
    };
    return { showForResult, resolve, reject };
  }, []);

  return {
    open,
    state,
    setState,
    ...memoizedFunctions,
  };
};

export default usePopupState;
